# vue3-tiger-admin


### 1. 背景

 先看一下我们完成的效果

- [vue3-tiger-admin - Preivew](http://jsfront.gitee.io/vue3-tiger-admin)
- [vue3-tiger-admin - Gitee](https://gitee.com/jsfront/vue3-tiger-admin)

### 2. 相关介绍

#### 2.1 基本介绍
- `vue3-tiger-admin` 之外还有 [react-tiger-admin](https://gitee.com/jsfront/react-tiger-admin)。
-  本项目全部由 `vite` 开发编译打包后 `dist` 目录发布到 [gitee pages](https://gitee.com/help/articles/4136) 上。


#### 2.2 技术栈

技术 | 说明 | 官网
----|----|----
Vite | 下一代前端开发与构建工具 | [https://cn.vitejs.dev/](https://cn.vitejs.dev/)
Vue3 | 渐进式JavaScript 框架 | [https://vuejs.org/](https://vuejs.org/)
Vue-router | 官方的路由管理器 | [https://router.vuejs.org/zh/](https://router.vuejs.org/zh/)
Typescript |  类型约束 | [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
Pinia | 全局状态管理模式 | [https://pinia.vuejs.org/](https://pinia.vuejs.org/)
Axios | 基于promise 的HTTP 库 | [https://github.com/axios/axios](https://github.com/axios/axios)
Arco-Vue | UI组件库 | [https://arco.design/vue/docs/start](https://arco.design/vue/docs/start)
Vueuse | 一套高质量的 Vue Hooks 库 | [https://vueuse.org/](https://vueuse.org/)
Easy-mock-bookset | 简单好用的在线接口 MOCK 平台 | [https://easy-mock.bookset.io/](https://easy-mock.bookset.io/)
Dayjs | JavaScript 日期处理类库 | [https://day.js.org/](https://day.js.org/)
SCSS | CSS预处理器 | [https://sass-lang.com/](https://sass-lang.com/)
Tinymce | 可视化HTML编辑器 | [https://www.tiny.cloud/](https://www.tiny.cloud/)


### 3.  开发步骤

#### 3.1. 插件安装
先安装 `Vscode` 插件

- [Volar - Explore high-performance tooling for Vue](https://github.com/johnsoncodehk/volar)
- [TypeScript Vue Plugin (Volar)](https://github.com/johnsoncodehk/volar/tree/master/extensions/vscode-typescript-vue-plugin)

否则会报很多莫明的错误。

#### 3.2. 快速启动

- node 版本管理工具

    - [nvm](https://github.com/nvm-sh/nvm)
    - [nvm-windows](https://github.com/coreybutler/nvm-windows)

- 备选工具

    - [node 版本管理工具 - n](https://github.com/tj/n)
    - [npm 源管理工具 - nrm](https://github.com/Pana/nrm)

- 教程

    - [使用 nvm 管理不同版本的 node 与 npm](https://www.runoob.com/w3cnote/nvm-manager-node-versions.html)
    - [npm 源管理器 nrm 使用教程](https://segmentfault.com/a/1190000017419993)


```
// 切换环境
nvm install 16.0.0
nvm use 16.0.0

// 安装依赖
npm install

// 启动项目
npm start

// 清除 node_modules
npm run clean

// 全局安装 rimraf 之后方可使用
npm i rimraf -g

// 清除 node_modules 重新安装依赖
// 等同于 npm run clean && npm install
npm run reinstall

```

如果还想找回此文，您可以点击右上角 💖 Star 💖 收藏 + 支持
