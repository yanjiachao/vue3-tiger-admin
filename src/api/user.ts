import request from '@/utils/request'
import type { UserState } from '@/store/modules/user/types'

export interface LoginData {
  username: string
  password: string
  imgCode: string
}

export interface LoginRes {
  token: string
}
export function login(data: LoginData) {
  return request.post<LoginRes>('/user/login', data)
}

export function logout() {
  return request.post<LoginRes>('/user/logout')
}

export function getUserInfo() {
  return request.post<UserState>('/user/info')
}
