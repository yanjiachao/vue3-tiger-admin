import type { AxiosRequestConfig } from 'axios'

export interface AnyObject {
  [key: string]: unknown
}

export interface Options {
  value: unknown
  label: string
}

export interface CustomAxiosRequestConfig extends AxiosRequestConfig {
  hideLoading?: boolean
}
